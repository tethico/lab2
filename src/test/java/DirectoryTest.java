import fs.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DirectoryTest {
    @Test
    public void getContent_getContentEmpty_contentIsEmpty() {
        Directory root = new Directory("directory");
        assertEquals(Collections.emptyList(), root.getContent());
    }

    @Test
    public void insertAndGetContent_insertRightContent_contentExists() {
        Directory root = new Directory("root");
        File binFile = new BinaryFile("file.bin");
        File bufFile  = new BufferFile("file.bf");
        Directory dataDir = new Directory("data");
        Directory logsDir = new Directory("logs");
        File logFile = new LogFile("file.log");

        root.insert(binFile);
        root.insert(bufFile);
        root.insert(dataDir);
        dataDir.insert(logsDir);
        logsDir.insert(logFile);

        assertEquals(Arrays.asList(binFile, bufFile, dataDir), root.getContent());
        assertEquals(Collections.singletonList(logsDir), dataDir.getContent());
        assertEquals(Collections.singletonList(logFile), logsDir.getContent());
    }

    @Test
    public void insertAndDelete_insertRightContent_contentIsDeleted() {
        Directory root = new Directory("root");
        Directory dataDir = new Directory("data");
        File bufFile  = new BufferFile("file.bf");
        File binFile = new BinaryFile("file.bin");

        root.insert(dataDir);
        dataDir.insert(bufFile);
        dataDir.insert(binFile);

        bufFile.delete();
        assertEquals(Collections.singletonList(binFile), dataDir.getContent());

        dataDir.delete();
        assertEquals(Collections.emptyList(), root.getContent());
    }

    @Test
    public void search_ByPattern_contentExists() {
        Directory root = new Directory("root");
        Directory dataDir = new Directory("data");
        File bufFile  = new BufferFile("file.bf");
        File binFile = new BinaryFile("file.bin");

        root.insert(dataDir);
        dataDir.insert(bufFile);
        dataDir.insert(binFile);

        List<String> actualPath = root.search("file");

        List<String> expectedPath = new ArrayList<>();
        expectedPath.add("/root/data/file.bf");
        expectedPath.add("/root/data/file.bin");

        assertTrue(expectedPath.equals(actualPath));

    }

    @Test
    public void count_ByPattern_contentExists() {
        Directory root = new Directory("root");
        Directory dataDir = new Directory("data");
        File bufFile  = new BufferFile("file.bf");
        File binFile = new BinaryFile("file.bin");

        root.insert(dataDir);
        dataDir.insert(bufFile);
        dataDir.insert(binFile);

        Long count = dataDir.count("file", true);

        assertEquals(2, (long)count);
    }

    @Test
    public void count_ByRecursive_contentExists() {
        Directory root = new Directory("root");
        Directory dataDir = new Directory("data");
        File bufFile  = new BufferFile("file.bf");
        File binFile = new BinaryFile("file.bin");

        root.insert(dataDir);
        dataDir.insert(bufFile);
        dataDir.insert(binFile);

        Long count = dataDir.count(true);

        assertEquals(2l, (long) count);
    }
}

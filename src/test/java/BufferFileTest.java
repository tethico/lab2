import fs.BufferFile;
import fs.Directory;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class BufferFileTest {

    @Test
    public void read_readEmptyFile_contentIsNull(){
        BufferFile file = new BufferFile("file.bf");
        assertNull(file.read());
    }

    @Test
    public void read_containsOneElement_contentNotEmpty(){
        BufferFile file = new BufferFile("file.bf");
        file.write("first");

        assertEquals("first", file.read());
        assertNull(file.read());
    }

    @Test
    public void read_containsManyElements_contentNotEmpty(){
        BufferFile file = new BufferFile("file.bf");
        for (int i = 0; i < 10; i++) {
            file.write("element #" + i);
        }

        for (int i = 0; i < 10; i++) {
            assertEquals("element #" + i, file.read());
        }

        assertNull(file.read());
    }

    @Test
    public void getParentDirectory_fileWithoutParentDirectory_parentDirectoryIsNull() {
        BufferFile file = new BufferFile("file.bf");
        assertNull(file.getParentDirectory());
    }

    @Test
    public void getParentDirectory_fileWithParentDirectory_parentDirectoryIsNotNull() {
        Directory root = new Directory("directory");
        BufferFile file = new BufferFile("file.bf");
        file.move(root);
        assertEquals(root, file.getParentDirectory());
    }
}

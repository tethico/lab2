import fs.Directory;
import fs.LogFile;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class LogFileTest {

    @Test
    public void read_emptyFile_contentIsNull(){
        LogFile file = new LogFile("file.log");
        assertEquals("", file.read());
    }

    @Test
    public void read_containsOneElement_contentNotEmpty(){
        LogFile file = new LogFile("file.log");

        file.write("first");

        assertEquals("first", file.read());

        // ensure that file has content after previous read
        assertEquals("first", file.read());
    }

    @Test
    public void read_containsSomeElements_contentNotEmpty(){
        LogFile file = new LogFile("file.log");

        String[] strings = { "one", "two", "three" };

        for (String item: strings) {
            file.write(item);
        }

        String expectedContent = String.join("\n", strings);
        assertEquals(expectedContent, file.read());
        assertEquals(expectedContent, file.read());
    }

    @Test
    public void read_containsSomeElementsAndAddNewAfterRead_contentNotEmpty(){
        LogFile file = new LogFile("file.log");

        String[] strings = { "one", "two", "three" };
        List<String> lines = Arrays.asList(strings);

        for (String item: lines) {
            file.write(item);
        }

        assertEquals(String.join("\n", lines), file.read());

        file.write("newLine");
        lines.add("newLine");
        assertEquals(String.join("\n", lines), file.read(), file.read());
    }

    @Test
    public void getParent_fileWithoutParent_parentIsNull() {
        LogFile file = new LogFile("file.log");
        assertNull(file.getParentDirectory());
    }

    @Test
    public void getParent_fileWithParent_parentIsNotNull() {
        Directory root = new Directory("directory");
        LogFile file = new LogFile("file.bf");
        file.move(root);
        assertEquals(root, file.getParentDirectory());
    }
}

import fs.BinaryFile;
import fs.Directory;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class BinaryFileTest {

    @Test
    public void read_readEmptyContent_contentIsEmpty() {
        BinaryFile file = new BinaryFile("file.bin");
        assertEquals("", file.read());

        file = new BinaryFile("file.bin", "");
        assertEquals("", file.read());
    }

    @Test
    public void read_readContent_rightContentExists() {
        BinaryFile file = new BinaryFile("file.bin", "content exists");
        assertEquals("content exists", file.read());
    }

    @Test
    public void getParentDirectory_fileWithoutParentDirectory_parentDirectoryIsNull() {
        BinaryFile file = new BinaryFile("file.bin", "content exists");
        Directory parent = file.getParentDirectory();
        assertNull(parent);
    }

    @Test
    public void getParentDirectory_fileWithParentDirectory_parentDirectoryIsNotNull() {
        Directory root = new Directory("directory");
        BinaryFile file = new BinaryFile("file.bin");
        file.move(root);
        assertEquals(root, file.getParentDirectory());
    }
}

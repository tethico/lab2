package fs;

import java.util.ArrayList;
import java.util.List;

public interface IFile {

    String getName();

    Directory getParentDirectory();

    void move(Directory newDirectory);

    void delete();

    String getDescription();

    String getPath();
}

package fs;


import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class File implements IFile{
    private String name;
    private Directory parentDirectory;
    protected Lock mutex ;

    public File(String name) {
        this.name = name;
        mutex = new ReentrantLock();
    }

    @Override
    public String getName() {
        return name;
    }

    public Directory getParentDirectory() {
        return parentDirectory;
    }

    protected void setParentDirectory(Directory newDirectory) {
        try {
            mutex.lock();
            this.parentDirectory = newDirectory;
        } finally {
            mutex.unlock();
        }
    }

    public void move(Directory newDirectory) {
        try {
            mutex.lock();
            if (parentDirectory != null)
                parentDirectory.deleteFile(this);
            newDirectory.insert(this);
        } finally {
            mutex.unlock();
        }
    }

    public void delete() {
        try {
            mutex.lock();
            if (parentDirectory != null) {
                parentDirectory.deleteFile(this);
            }
        } finally {
            mutex.unlock();
        }
    }

    public String getDescription() {
        return getName() + " (" + getClass().getSimpleName() + ")";
    }

    public String getPath() {
        if (parentDirectory == null) return "/" + getName();
        return ((File)parentDirectory).getPath() + "/" + getName();
    }
}


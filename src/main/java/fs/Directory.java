package fs;

import fs.FJTasks.CountTask;
import fs.FJTasks.JsonTreeTask;
import fs.FJTasks.SearchTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ForkJoinPool;

public class Directory extends File {
    private static final int DIR_MAX_ELEMENT = 100;

    private Set<File> children;

    public Directory(String name) {
        super(name);
        children = new LinkedHashSet<>();
    }

    public List<File> getContent() {
        try {
            mutex.lock();
            return new ArrayList<>(children);
        } finally {
            mutex.unlock();
        }
    }

    public void insert(File child) {
        try {
            mutex.lock();
            if (children.size() < DIR_MAX_ELEMENT) {
                children.add(child);
                child.setParentDirectory(this);
            }
        } finally {
            mutex.unlock();
        }
    }

    public void deleteFile(File child) {
        try {
            mutex.lock();
            children.remove(child);
        } finally {
            mutex.unlock();
        }
    }

    public List<String> search(String pattern){
        return  new ForkJoinPool().invoke(new SearchTask(this, pattern));
    }

    public Long count(boolean recursive){
        if(recursive){
            return new ForkJoinPool().invoke(new CountTask(this));
        }else{
            return (long) getContent().size();
        }
    }

    public String tree() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(new ForkJoinPool().invoke(new JsonTreeTask(this)));
    }

    public long count(String pattern, boolean recursive){
        if(recursive){
            return (long) search(pattern).size();
        }else{
            return getContent().stream().filter(x -> x.getName().contains(pattern)).count();
        }
    }
}

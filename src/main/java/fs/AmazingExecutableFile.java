package fs;

public class AmazingExecutableFile extends File implements Runnable {
    private LambdaFunction func;
    private AmazingExecutableFile(String name, LambdaFunction func) {
        super(name);
        this.func = func;
    }

    public static AmazingExecutableFile create(String name, LambdaFunction func) {
        return new AmazingExecutableFile(name, func);
    }

    @Override
    public void run() {
        func.Call();
    }
}


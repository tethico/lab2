package fs;

@FunctionalInterface
public interface LambdaFunction {
    void Call();
}
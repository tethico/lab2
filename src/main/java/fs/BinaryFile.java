package fs;


public class BinaryFile extends File {
    private String content;

    public BinaryFile(String name) {
        super(name);
        this.content = "";
    }

    public BinaryFile(String name, String content) {
        super(name);
        this.content = content;
    }

    public String read() {
        return content;
    }
}

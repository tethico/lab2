package fs;

import java.util.ArrayDeque;
import java.util.Queue;

public class BufferFile extends File {
        private static final int MAX_BUF_FILE_SIZE = 100;
        private Queue<String> content;

        public BufferFile(String name) {
            super(name);
            this.content = new ArrayDeque<>();
        }

        public void write(String newLine) {
            try {
                mutex.lock();
                if (content.size() > MAX_BUF_FILE_SIZE)
                    return;
                content.add(newLine);
            } finally {
                mutex.unlock();
            }
        }

        public String read() {
            return content.poll();
        }
    }

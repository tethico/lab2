package fs;


import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class LogFile extends File {
    private Queue<String> content;
    private ReadWriteLock readWriteLock;
    public LogFile(String name) {
        super(name);
        content = new ArrayDeque<>();
        readWriteLock = new ReentrantReadWriteLock();
    }

    public void write(String newLine) {
        Lock mutex = readWriteLock.writeLock();
        try {
            mutex.lock();
            content.add(newLine);
        } finally {
            mutex.unlock();
        }
    }

    public String read() {
        Lock mutex = readWriteLock.readLock();
        try {
            mutex.lock();
            return String.join("\n", content);
        } finally {
            mutex.unlock();
        }
    }
    }

